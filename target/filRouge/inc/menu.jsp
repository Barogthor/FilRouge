<%--
  Created by IntelliJ IDEA.
  User: meg4r0m
  Date: 11/03/18
  Time: 18:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="menu">
    <p><a href="<c:url value="/ClientCreate"/>">Créer un nouveau client</a></p>
    <p><a href="<c:url value="/CommandCreate"/>">Créer une nouvelle commande</a></p>
    <p><a href="<c:url value="/ClientsList"/>">Voir les clients existants</a></p>
    <p><a href="<c:url value="/CommandsList"/>">Voir les commandes existantes</a></p>
</div>
